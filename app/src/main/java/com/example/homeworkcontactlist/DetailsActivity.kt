package com.example.homeworkcontactlist

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.ScaleGestureDetector
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_details.*
import android.view.MotionEvent

class DetailsActivity: AppCompatActivity() {

    private var entry_id = -1
    lateinit var contacts: ArrayList<Contact>

    // Pinch zoom will occurred on this image widget.
    private var imageView: ImageView? = null

    // Used to detect pinch zoom gesture.
    private var scaleGestureDetector: ScaleGestureDetector? = null

    companion object {
        var ENTRY_ID = "ID"
        var entry_id = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        getActionBar()?.setDisplayHomeAsUpEnabled(true)

        // if intent has extras, get extra and save as static
        // if not - load from static
        if(intent.hasExtra(ENTRY_ID)) {
            entry_id = intent.extras!!.get(ENTRY_ID) as Int
            DetailsActivity.entry_id = entry_id
        } else {
            entry_id = DetailsActivity.entry_id
        }
        contacts = Contact.getAllContacts()

        // set content
        userName.text = contacts[entry_id].FullName
        userImage.setImageResource(contacts[entry_id].ImageId ?: R.drawable.icon_1)
        userEmail.text = contacts[entry_id].Email
        userAge.text = "Age: ${contacts[entry_id].Age}"
        userCity.text = "City: ${contacts[entry_id].City}"
        userDetails.text = contacts[entry_id].Details

        initControls()

        userImage.setOnClickListener {
            val intent = Intent(baseContext, ImageActivity::class.java)
            intent.putExtra(ImageActivity.ENTRY_ID, entry_id)
            startActivity(intent)
        }
    }

    private fun initControls()
    {
        if(imageView == null)
        {
            // Get the image view in the layout xml.
            imageView = userImage
        }

        if(scaleGestureDetector == null)
        {
            // Create an instance of OnPinchListner custom class.
            var onPinchListener = OnPinchListener(getApplicationContext(), imageView ?: userImage)

            // Create the new scale gesture detector object use above pinch zoom gesture listener.
            scaleGestureDetector = ScaleGestureDetector(getApplicationContext(), onPinchListener)
        }
    }
    override fun onTouchEvent(event: MotionEvent): Boolean {
        // Dispatch activity on touch event to the scale gesture detector.
        scaleGestureDetector?.onTouchEvent(event)
        return true
    }
}

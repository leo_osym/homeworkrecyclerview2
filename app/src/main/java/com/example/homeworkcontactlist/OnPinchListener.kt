package com.example.homeworkcontactlist

import android.content.Context
import android.util.Log
import android.view.ScaleGestureDetector
import android.widget.ImageView
import android.widget.Toast
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.drawable.BitmapDrawable

class OnPinchListener(context: Context, srcImageView: ImageView): ScaleGestureDetector.SimpleOnScaleGestureListener() {


    companion object {
        private var TAG_PINCH_LISTENER: String = "PINCH_LISTENER"
    }

    // Pinch zoon occurred on this image view object.
    private var srcImageView: ImageView? = null

    // Related appication context.
    private var context: Context? = null
    init {
        this.context = context
        this.srcImageView = srcImageView
    }

    // When pinch zoom gesture occurred.
    override public fun onScale(detector: ScaleGestureDetector?): Boolean{

        if(detector != null) {

            var scaleFactor: Float = detector.getScaleFactor()

            if (srcImageView != null) {

                // Scale the image with pinch zoom value.
                scaleImage(scaleFactor, scaleFactor);

            } else {
                if (context != null) {
                    Toast.makeText(context, "", Toast.LENGTH_SHORT).show()
                } else {
                    Log.e(TAG_PINCH_LISTENER, "Both context and srcImageView is null.")
                }
            }
        }else
        {
            Log.e(TAG_PINCH_LISTENER, "Pinch listener onScale detector parameter is null.")
        }

        return true
    }

    /* This method is used to scale the image when user pinch zoom it. */
    private fun scaleImage(xScale: Float, yScale: Float)
    {
        // Get source image bitmap object.
        var srcBitmapDrawable: BitmapDrawable = (srcImageView?.drawable as BitmapDrawable)
        var srcBitmap = srcBitmapDrawable.getBitmap()

        // Get source image width and height.
        var srcImageWith = srcBitmap.getWidth()
        var srcImageHeight = srcBitmap.getHeight()

        // Get source image config object.
        var srcImageConfig: Bitmap.Config = srcBitmap.getConfig()

        // Create a new bitmap which has scaled width and height value from source bitmap.
        var scaleBitmap = Bitmap.createBitmap((srcImageWith * xScale).toInt(),
            (srcImageHeight * yScale).toInt(), srcImageConfig)

        // Create the scaled canvas.
        var scaleCanvas = Canvas(scaleBitmap)

        // Create the Matrix object which will scale the source bitmap to target.
        var scaleMatrix = Matrix()

        // Set x y scale value.
        scaleMatrix.setScale(xScale, yScale)

        // Create a new paint object.
        var paint = Paint()

        // Draw the new scaled bitmap in the canvas.
        scaleCanvas.drawBitmap(srcBitmap, scaleMatrix, paint);

        // Display the new scaled bitmap to source image view object.
        srcImageView?.setImageBitmap(scaleBitmap)
    }

}
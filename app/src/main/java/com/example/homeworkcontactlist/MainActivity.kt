package com.example.homeworkcontactlist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent

class MainActivity : AppCompatActivity() {

    lateinit var contacts: ArrayList<Contact>
    lateinit var adapter: ContactsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        contacts = Contact.getAllContacts()
        adapter = ContactsAdapter(contacts, this)
        // add separator line
        recyclerView.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        recyclerView.layoutManager = LinearLayoutManager(this)

        adapter.setListener(object : ContactsAdapter.Listener {
            override fun onClick(position: Int) {
                val intent = Intent(baseContext, DetailsActivity::class.java)
                intent.putExtra(DetailsActivity.ENTRY_ID, position)
                startActivity(intent)
                //Toast.makeText(baseContext, contacts[position].FullName, Toast.LENGTH_SHORT).show()
            }
        })
        recyclerView.adapter = adapter
    }
}

package com.example.homeworkcontactlist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_image.*

class ImageActivity : AppCompatActivity() {

    companion object {
        var ENTRY_ID = "ID"
    }
    private var entry_id = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)
        getActionBar()?.setDisplayHomeAsUpEnabled(true)

        entry_id = intent.extras!!.get(DetailsActivity.ENTRY_ID) as Int
        var res: Int? = Contact.getAllContacts()[entry_id].ImageId
        imageView.setImageResource(res ?: R.drawable.icon_1)
    }
}

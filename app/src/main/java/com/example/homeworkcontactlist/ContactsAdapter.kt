package com.example.homeworkcontactlist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.contact_layout.view.*
import android.R.attr.onClick



class ContactsAdapter(val items : ArrayList<Contact>, val context: Context):
    RecyclerView.Adapter<ContactsAdapter.ViewHolder>() {

    // add interface-listener to move intent functionality to MainActivity
    private lateinit var listener: Listener
    interface Listener {
        fun onClick(position: Int)
    }

    fun setListener(listener: Listener) {
        this.listener = listener
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.userName?.text = items.get(position).FullName
        holder.userEmail?.text = items.get(position).Email
        holder.userIcon?.setImageResource(items.get(position).ImageId ?: R.drawable.icon_1)

        holder.itemView.setOnClickListener{
            listener.onClick(position)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.contact_layout, parent, false))
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val userName = itemView.user_name
        val userEmail = itemView.user_email
        val userIcon = itemView.user_icon
    }
}